---
title: "Other Projects and presentations"
date: 2023-08-02T12:36:07+02:00
---

I try to report / fix bugs in Open Source Software as good and often as I can.
See my GitHub and Gitlab pages.

Some projects im more involved than the ocasional bug report or pull request:

* [Flarp Monthly meeting in Berlin and remote for sysadmins](https://www.flarp.de)
* [A list of IPv6 related resouces](https://gitlab.com/jenslink/ipv6-resources/)
* [A collaborative IPv6 book](https://github.com/becarpenter/book6)

# Presentations

* Markdown (Flarp, May 2023, German)
  [Slides](https://www.flarp.de/slides/markdown.pdf),
  [Video](https://opencast.hu-berlin.de/paella/ui/watch.html?id=f319509b-87e4-43f3-88d2-1517d7d8235c)
