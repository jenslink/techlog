---
title: "About/Imprint"
date: 2023-08-02T12:36:07+02:00
---

A Blog about IPv6, Linux, networking, technology and other stuff.

Follow me on [Mastodon](https://mastodon.social/@quux) or [Twitter](https://www.twitter.com/QuuxBerlin).

# Imprint

    Jens Link
    12051 Berlin
    Germany
    (Full address provided via eMail if legally required)

    eMail: techlog@jenslink.net

# Data protection

If you are missing cookie warnings: there are none.

Logs will be kept for 5 days and only be used for troubleshooting, detecting
malicious activity and once a day to count IPv4 vs. IPv6 connections.
