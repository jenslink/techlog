---
title: "Support"
date: 2023-08-02T12:36:07+02:00
---

If you like to support me, this blog and the soon-to- be created YouTube
channel you can:

* contribute by reporting / fixing typos, bugs broken links and co on
  [gitlab](https://gitlab.com/jenslink/techlog/) either via issuse tracer or
  merge request
* hire me IPv6, Linux and Ansible related projects or workshops: techlog@jenslink.net
* support me on [Patreon](https://patreon.com/JensTechlog)
