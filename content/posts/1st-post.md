---
title: "1st Post"
date: 2023-08-11T15:15:00+02:00
---

Welcome to my new blog. Yes, a new blog n August of 2023. Since
the last post in my old blog in February 2019 I moved to the other
side of Berlin, worked on several interesting projects and accumulated
quite a lot of ideas of things I should document or learn about and then
document.

# What to expect

So as I'm an old nerd, who is using Linux since he first got it an 35 3.5"
floppy disks[^1], worked with networking equipment since around 2002 and
has IPv6 running on his own server(s) since 2007, you can expect quite
a lot Linux, Networking and IPv6 content. Also, there may be some posts
about home automation, sensors and other electronics / IoT topics.

I also try to have two posts a week: one longer one on Sundays, and a
collection of URLs I ran across during the week on Thursdays.

[^1]: For younger readers: Old storage media with 1.44 MB capacity, today
only used as "save" icon in some GUI applications.
