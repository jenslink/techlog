---
title: "Mollyguard"
date: 2023-08-13T10:00:00+02:00
tags: ["linux", "debian", "ubuntu", "tools"]
---
Have you ever accidentally typed *shutdown* or *reboot* in the wrong terminal
window and shutdown or rebooted the wrong server? I guess it happens even to
the most careful admins out there.

One of my default packages I install on Debian GNU/Linux and Ubuntu by default
is molly-guard.

(https://en.wiktionary.org/wiki/molly-guard)

So when you now type reboot or shutdown when connected via ssh you'll be
prompted to enter the host name. If you work locally the host will just reboot or
shutdown.
