---
title: "What to use in Documentation"
date: 2023-08-11T15:25:00+02:00
tags: [IPv4,IPv6,BGP,DNS]
---

You want to make a presentation, write a blog post or document your
brand-new software.

What do you use as domain-names, IP addresses and AS numbers? If you are
documenting your systems or a setup you build for a customer the question is
easy to answer: It's best to use the actual data. But if you are writing a more
generic documentation there are some reserved domain names, IP prefixes and
AS numbers.

# Why

But first let's ask why we should use specialized values and not some random
stuff you made just up.

People like to copy stuff from documentation. At least a couple of years
ago "cisco" was the most common password on any Cisco router or Switch. No it
was not a default password. But it was the password that was in every
documentation. Either from on the website or any of the training materials[^1]

If you believe rumors a big German government agency is another example for that.
They use the IPv4 address range from an university. Not because
they randomly picked a prefix, but because before they implemented IPv4
(and yes there was a time before IP!) they had training and consulting and the
training materials where written by a student form that university. So they
took the prefix they knew from the documentation and used it for their IP
addressing plan.

It is also bad idea to just make up domain names. Years ago there was a very
controversial article in a magazine with a Domain name as a title. A friend
noticed that the domain wasn't reserved, registered it and published an
article describing what was wrong with the article in the magazine.

Some time ago I also saw some advertising showing a domain-name which was
pointing to the website of a competitor.

# What

## IPv4 prefixes

* 192.0.2.0/24
* 198.51.100.0/24
* 203.0.113.0/24

[RFC5737](https://datatracker.ietf.org/doc/html/rfc5737)

## IPv6 prefix

* IPv6  2001:db8::/32 (Unfortunately there is currently only on IPv6 prefix reserved)

[RFC3849](https://datatracker.ietf.org/doc/html/rfc3849)

## Domain names

* example.com
* example.net
* example.org

[RFC2606](https://www.rfc-editor.org/rfc/rfc2606.html)

## AS numbers

* 64496 - 64511 (16Bit)
* 65536 - 65551 (32Bit)

[RFC5398](https://www.rfc-editor.org/rfc/rfc5398.txt)
[^1]: Except of maybe the IOS security training where you where told that you shouldn't
    use "cisco" as a password.

